package br.com.concretesolutions.dribble;

import android.test.ActivityInstrumentationTestCase2;

import br.com.concretesolutions.dribble.activities.MainActivity_;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.Matchers.anything;

public class ApplicationTest extends ActivityInstrumentationTestCase2<MainActivity_> {

	public ApplicationTest() {
		super(MainActivity_.class);
	}

	@Override
	public void setUp() throws Exception {
		super.setUp();
		getActivity();
	}

	public void testApp() {
		// Check if shots list is displayed
		onView(withId(R.id.listView)).check(matches(isDisplayed()));
		// Perform a click on first position
		onData(anything()).inAdapterView(withId(R.id.listView)).atPosition(0).perform(click());
		// Check if player's name is displayed on shot detail screen
		onView(withId(R.id.shotNamePlayer)).check(matches(isDisplayed()));
		// Perform a click on shot's image
		onView(withId(R.id.shotImage)).perform(click());
		// Check if the image was displayed and then click on it
		onView(withId(R.id.fullScreenImage)).check(matches(isDisplayed())).perform(click());
	}

}