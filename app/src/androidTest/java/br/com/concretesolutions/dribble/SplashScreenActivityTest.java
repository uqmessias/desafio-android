package br.com.concretesolutions.dribble;

import android.test.ActivityInstrumentationTestCase2;

import br.com.concretesolutions.dribble.activities.SplashScreenActivity_;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Created by tpinho on 5/17/15.
 */
public class SplashScreenActivityTest extends ActivityInstrumentationTestCase2<SplashScreenActivity_> {

	public SplashScreenActivityTest() {
		super(SplashScreenActivity_.class);
	}

	@Override
	public void setUp() throws Exception {
		super.setUp();
		getActivity();
	}

	public void testImageIsDisplayed() {
		// Check if logo image is displayed
		onView(withId(R.id.logo)).check(matches(isDisplayed()));
	}

}