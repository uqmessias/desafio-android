package br.com.concretesolutions.dribble.activities;

import android.app.Activity;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

import br.com.concretesolutions.dribble.R;

/**
 * Created by tpinho on 5/17/15.
 */
@EActivity(R.layout.activity_full_screen_image)
public class FullScreenImageActivity extends Activity {

	@Extra
	String url;

	@ViewById
	ImageView fullScreenImage;

	@AfterViews
	public void afterViews() {
		Picasso.with(this)
				.load(url)
				.placeholder(R.drawable.shot_placeholder)
				.into(fullScreenImage);
	}

	@Click
	public void fullScreenImageClicked() {
		finish();
	}

}