package br.com.concretesolutions.dribble.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import br.com.concretesolutions.dribble.R;

/**
 * Created by tpinho on 5/17/15.
 */
@EViewGroup(R.layout.view_empty_list)
public class EmptyListView extends RelativeLayout {

	@ViewById
	TextView emptyList;

	public EmptyListView(Context context) {
		super(context);
	}

	public EmptyListView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public EmptyListView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	public EmptyListView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
	}

}