package br.com.concretesolutions.dribble.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by tpinho on 5/17/15.
 */
public class Shot implements Serializable {

	private Long id;

	private String title;

	private String description;

	@SerializedName("image_url")
	private String imageUrl;

	private String url;

	@SerializedName("views_count")
	private int viewsCount;

	private Player player;

	public Long getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public int getViewsCount() {
		return viewsCount;
	}

	public String getUrl() {
		return url;
	}

	public Player getPlayer() {
		return player;
	}

}