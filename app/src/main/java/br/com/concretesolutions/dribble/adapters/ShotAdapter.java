package br.com.concretesolutions.dribble.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.util.ArrayList;
import java.util.List;

import br.com.concretesolutions.dribble.models.Shot;
import br.com.concretesolutions.dribble.views.ShotView;
import br.com.concretesolutions.dribble.views.ShotView_;

/**
 * Created by tpinho on 5/17/15.
 */
@EBean
public class ShotAdapter extends BaseAdapter {

	List<Shot> shots;

	@RootContext
	Context context;

	@AfterInject
	void initAdapter() {
		shots = new ArrayList<>();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ShotView shotView;

		if (convertView == null) {
			shotView = ShotView_.build(context);
		} else {
			shotView = (ShotView) convertView;
		}

		shotView.bind(getItem(position), false);

		return shotView;
	}

	@Override
	public int getCount() {
		return shots.size();
	}

	@Override
	public Shot getItem(int position) {
		return shots.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public void addAll(List<Shot> items) {
		shots.addAll(items);
	}

}