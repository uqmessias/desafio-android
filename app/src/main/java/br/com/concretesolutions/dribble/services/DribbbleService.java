package br.com.concretesolutions.dribble.services;

import br.com.concretesolutions.dribble.models.Shot;
import br.com.concretesolutions.dribble.models.ShotsWrapper;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by tpinho on 5/17/15.
 */
public interface DribbbleService {

	@GET("/shots/popular")
	void getPopularShots(@Query("page") int page, Callback<ShotsWrapper> callback);

	@GET("/shots/{id}")
	void getShot(@Path("id")long id, Callback<Shot> callback);

}