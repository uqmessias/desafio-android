package br.com.concretesolutions.dribble.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by tpinho on 5/17/15.
 */
public class Player implements Serializable {

	private String name;

	@SerializedName("avatar_url")
	private String avatarUrl;

	public String getName() {
		return name;
	}

	public String getAvatarUrl() {
		return avatarUrl;
	}

}