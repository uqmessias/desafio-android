package br.com.concretesolutions.dribble.activities;

import android.view.View;
import android.widget.ListView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.ViewById;

import br.com.concretesolutions.dribble.R;
import br.com.concretesolutions.dribble.adapters.ShotAdapter;
import br.com.concretesolutions.dribble.listeners.PaginationScrollListener;
import br.com.concretesolutions.dribble.models.Shot;
import br.com.concretesolutions.dribble.models.ShotsWrapper;
import br.com.concretesolutions.dribble.services.DribbbleService;
import br.com.concretesolutions.dribble.services.callbacks.RetrofitCallback;
import br.com.concretesolutions.dribble.services.factories.ServiceFactory;
import br.com.concretesolutions.dribble.views.EmptyListView;
import retrofit.client.Response;

/**
 * Created by tpinho on 5/17/15.
 */
@EActivity(R.layout.activity_main)
public class MainActivity extends BaseActivity {

	@ViewById
	ListView listView;

	@Bean
	ShotAdapter shotAdapter;

	@ViewById
	EmptyListView emptyListView;

	private PaginationScrollListener paginationScrollListener;

	@AfterViews
	void afterViews() {
		listView.setAdapter(shotAdapter);
		paginationScrollListener = new PaginationScrollListener() {
			@Override
			public void onLoadMore(int page, int totalItemsCount) {
				loadShots(page);
			}
		};

		listView.setOnScrollListener(paginationScrollListener);

		if (shotAdapter.getCount() == 0)
			loadShots(1);
	}

	@ItemClick
	void listViewItemClicked(Shot shot) {
		ShotDetailActivity_.intent(this).shot(shot).start();
	}

	private void loadShots(final int pageNumber) {
		showProgressDialog();
		DribbbleService service = ServiceFactory.createDribbleService(this);
		service.getPopularShots(pageNumber, new RetrofitCallback<ShotsWrapper>(this) {
			@Override
			public void success(ShotsWrapper shotsWrapper, Response response) {
				if (shotsWrapper != null) {
					emptyListView.setVisibility(View.GONE);
					paginationScrollListener.setDisabled(shotsWrapper.getPage() == shotsWrapper.getPages());
					shotAdapter.addAll(shotsWrapper.getShots());
					shotAdapter.notifyDataSetChanged();
				}

				dismissProgressDialog();
			}
		});
	}

}