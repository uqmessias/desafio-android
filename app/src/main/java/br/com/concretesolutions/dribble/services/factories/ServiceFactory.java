package br.com.concretesolutions.dribble.services.factories;

import android.content.Context;

import br.com.concretesolutions.dribble.services.DribbbleService;
import br.com.concretesolutions.dribble.utils.Utils;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;

/**
 * Created by tpinho on 5/17/15.
 */
public class ServiceFactory {

	private static final String URL_API = "https://api.dribbble.com";

	private ServiceFactory() {
	}

	private static RestAdapter createRestAdapter(final Context context) {
		RequestInterceptor requestInterceptor = new RequestInterceptor() {
			@Override
			public void intercept(RequestFacade request) {
				request.addHeader("Accept", "application/json");
				if (Utils.isNetworkAvaliable(context)) {
					int maxAge = 60;
					request.addHeader("Cache-Control", "public, max-age=" + maxAge);
				} else {
					int maxStale = 60 * 60 * 24 * 28;
					request.addHeader("Cache-Control", "public, only-if-cached, max-stale=" + maxStale);
				}
			}
		};

		RestAdapter restAdapter = new RestAdapter.Builder()
				.setLogLevel(RestAdapter.LogLevel.FULL)
				.setEndpoint(URL_API)
				.setRequestInterceptor(requestInterceptor)
				.build();

		return restAdapter;
	}

	public static DribbbleService createDribbleService(Context context) {
		return createRestAdapter(context).create(DribbbleService.class);
	}

}