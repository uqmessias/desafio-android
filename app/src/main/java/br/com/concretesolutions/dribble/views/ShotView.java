package br.com.concretesolutions.dribble.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import br.com.concretesolutions.dribble.R;
import br.com.concretesolutions.dribble.activities.FullScreenImageActivity_;
import br.com.concretesolutions.dribble.models.Shot;

/**
 * Created by tpinho on 5/17/15.
 */
@EViewGroup(R.layout.view_shot)
public class ShotView extends RelativeLayout {

	@ViewById
	ImageView shotImage;

	@ViewById
	TextView shotViewsCount;

	@ViewById
	TextView shotTitle;

	private Shot shot;

	public ShotView(Context context) {
		super(context);
	}

	public ShotView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public ShotView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	public ShotView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
	}

	public void bind(Shot shot, boolean enableCLickOnImage) {
		this.shot = shot;
		shotImage.setClickable(enableCLickOnImage);
		Picasso.with(getContext())
				.load(shot.getImageUrl())
				.placeholder(R.drawable.shot_placeholder)
				.into(shotImage);

		shotViewsCount.setText(String.valueOf(shot.getViewsCount()));
		shotTitle.setText(shot.getTitle());
	}

	@Click
	public void shotImageClicked() {
		FullScreenImageActivity_.intent(getContext()).url(shot.getImageUrl()).start();
	}

}