package br.com.concretesolutions.dribble.listeners;

import android.widget.AbsListView;

/**
 * Created by tpinho on 5/17/15.
 */
public abstract class PaginationScrollListener implements AbsListView.OnScrollListener {

	private int visibleThreshold = 5;
	private int currentPage = 0;
	private int previousTotalItemCount = 0;
	private boolean loading = true;
	private int startingPageIndex = 0;
	private boolean disabled;

	public PaginationScrollListener() {
	}

	public PaginationScrollListener(int visibleThreshold) {
		this.visibleThreshold = visibleThreshold;
	}

	public PaginationScrollListener(int visibleThreshold, int startPage) {
		this.visibleThreshold = visibleThreshold;
		this.startingPageIndex = startPage;
		this.currentPage = startPage;
	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
		if (!disabled) {
			if (totalItemCount < previousTotalItemCount) {
				currentPage = startingPageIndex;
				previousTotalItemCount = totalItemCount;
				if (totalItemCount == 0) {
					loading = true;
				}
			}

			if (loading && (totalItemCount > previousTotalItemCount)) {
				loading = false;
				previousTotalItemCount = totalItemCount;
				currentPage++;
			}

			if (!loading && (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)) {
				onLoadMore(currentPage + 1, totalItemCount);
				loading = true;
			}
		}
	}

	public abstract void onLoadMore(int page, int totalItemsCount);

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

}