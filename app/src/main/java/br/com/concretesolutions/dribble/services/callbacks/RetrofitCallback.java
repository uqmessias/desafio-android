package br.com.concretesolutions.dribble.services.callbacks;

import android.widget.Toast;

import br.com.concretesolutions.dribble.R;
import br.com.concretesolutions.dribble.activities.BaseActivity;
import retrofit.Callback;
import retrofit.RetrofitError;

/**
 * Created by tpinho on 5/17/15.
 */
public abstract class RetrofitCallback<T> implements Callback<T> {

	private BaseActivity baseActivity;

	public RetrofitCallback(BaseActivity baseActivity) {
		this.baseActivity = baseActivity;
	}

	@Override
	public void failure(RetrofitError error) {
		baseActivity.dismissProgressDialog();
		Toast.makeText(baseActivity, baseActivity.getString(R.string.error_connection), Toast.LENGTH_LONG).show();
	}

}