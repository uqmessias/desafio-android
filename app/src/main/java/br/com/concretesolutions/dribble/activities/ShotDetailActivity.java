package br.com.concretesolutions.dribble.activities;

import android.content.Intent;
import android.text.Html;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.ViewById;

import br.com.concretesolutions.dribble.R;
import br.com.concretesolutions.dribble.models.Shot;
import br.com.concretesolutions.dribble.services.DribbbleService;
import br.com.concretesolutions.dribble.services.callbacks.RetrofitCallback;
import br.com.concretesolutions.dribble.services.factories.ServiceFactory;
import br.com.concretesolutions.dribble.transforms.CircleTransform;
import br.com.concretesolutions.dribble.views.ShotView;
import retrofit.client.Response;

/**
 * Created by tpinho on 5/17/15.
 */
@EActivity(R.layout.activity_shot_detail)
public class ShotDetailActivity extends BaseActivity {

	@Extra
	Long shotId;

	@Extra
	Shot shot;

	@ViewById
	ShotView shotView;

	@ViewById
	ImageView shotAvatarPlayer;

	@ViewById
	TextView shotNamePlayer;

	@ViewById
	TextView shotDescription;

	@AfterViews
	void afterViews() {
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		loadShot();
	}

	private void loadShot() {
		if (shotId != null) {
			showProgressDialog();
			DribbbleService service = ServiceFactory.createDribbleService(this);
			service.getShot(shotId, new RetrofitCallback<Shot>(this) {
				@Override
				public void success(Shot shot, Response response) {
					bind(shot);
					dismissProgressDialog();
				}
			});
		} else if (shot != null) {
			bind(shot);
		}
	}

	private void bind(Shot shot) {
		this.shot = shot;
		getSupportActionBar().setTitle(shot.getTitle());
		shotView.bind(shot, true);
		Picasso.with(this)
				.load(shot.getPlayer().getAvatarUrl())
				.transform(new CircleTransform())
				.placeholder(R.drawable.avatar_placeholder)
				.into(shotAvatarPlayer);
		shotNamePlayer.setText(shot.getPlayer().getName());

		if (!TextUtils.isEmpty(shot.getDescription())) {
			shotDescription.setVisibility(View.VISIBLE);
			shotDescription.setText(Html.fromHtml(shot.getDescription()));
			shotDescription.setMovementMethod(LinkMovementMethod.getInstance());
		}
	}

	public void loadShot(Long id) {
		shotId = id;
		loadShot();
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		menu.findItem(R.id.action_share).setVisible(true);

		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_shot_detail, menu);
		return true;
	}

	@OptionsItem(R.id.action_share)
	public void shareAction() {
		Intent intent = new Intent();
		intent.setAction(Intent.ACTION_SEND);

		intent.setType("text/plain");
		intent.putExtra(Intent.EXTRA_TEXT, shot.getUrl());
		startActivity(Intent.createChooser(intent, getString(R.string.action_share)));
	}

	@OptionsItem(android.R.id.home)
	public void homeSelected() {
		onBackPressed();
	}

}