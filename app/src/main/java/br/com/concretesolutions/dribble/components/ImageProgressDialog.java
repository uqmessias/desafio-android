package br.com.concretesolutions.dribble.components;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import br.com.concretesolutions.dribble.R;

/**
 * Created by tpinho on 5/17/15.
 */
public class ImageProgressDialog extends ProgressDialog {

	private Animation animation;
	private ImageView imageView;

	public ImageProgressDialog(Context context) {
		super(context);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.image_progress_dialog);

		imageView = (ImageView) findViewById(R.id.animation);
		animation = AnimationUtils.loadAnimation(getContext(), R.anim.clockwise_rotation);
	}

	@Override
	public void show() {
		super.show();
		imageView.startAnimation(animation);
	}

	@Override
	public void dismiss() {
		super.dismiss();
		animation.cancel();
	}

}