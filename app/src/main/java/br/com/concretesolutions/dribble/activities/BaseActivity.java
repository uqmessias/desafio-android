package br.com.concretesolutions.dribble.activities;

import android.app.ProgressDialog;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;

import br.com.concretesolutions.dribble.components.ImageProgressDialog;

/**
 * Created by tpinho on 5/17/15.
 */
public class BaseActivity extends AppCompatActivity {

	private ImageProgressDialog progressDialog;

	public void showProgressDialog() {
		if (isFinishing() || getWindow().getDecorView() == null || progressDialog != null) {
			return;
		}

		dismissProgressDialog();
		progressDialog = new ImageProgressDialog(this);
		progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progressDialog.setIndeterminate(true);
		progressDialog.setCancelable(false);
		progressDialog.setCanceledOnTouchOutside(false);
		progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		progressDialog.show();
	}

	public void dismissProgressDialog() {
		if (progressDialog != null) {
			progressDialog.dismiss();
			progressDialog = null;
		}
	}

}