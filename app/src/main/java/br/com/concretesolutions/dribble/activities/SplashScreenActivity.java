package br.com.concretesolutions.dribble.activities;

import android.app.Activity;
import android.os.Handler;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.EActivity;

import br.com.concretesolutions.dribble.R;

/**
 * Created by tpinho on 5/17/15.
 */
@EActivity(R.layout.activity_splash_screen)
public class SplashScreenActivity extends Activity {

	@AfterInject
	void calledAfterInjection() {
		startSplashScreenThread();
	}

	private void startSplashScreenThread() {
		new Handler().postDelayed(new Runnable() {
			public void run() {
				finishSplashScreen();
			}
		}, 3000);
	}

	private void finishSplashScreen() {
		finish();
		MainActivity_.intent(this).start();
	}

}