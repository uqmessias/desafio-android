package br.com.concretesolutions.dribble.models;

import java.util.List;

/**
 * Created by tpinho on 5/17/15.
 */
public class ShotsWrapper {

	private int page;

	private int pages;

	private List<Shot> shots;

	public int getPage() {
		return page;
	}

	public int getPages() {
		return pages;
	}

	public List<Shot> getShots() {
		return shots;
	}

}